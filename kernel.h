#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <vector>
#include <stdio.h>
#include <ctime>

typedef unsigned int uint;
#define uintSize (sizeof(uint))

#define B2MB(x) ((x) / (1024 * 1024))
#define MB2B(x) ((x) * 1024 * 1024)

#define H2D cudaMemcpyHostToDevice
#define D2H cudaMemcpyDeviceToHost
#define D2D cudaMemcpyDeviceToDevice

#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define ELEMENT_WIDTH 32
#define ELEMENT_CARDINALITY (30 * 32)
#define BASE 30U
#define NEXT 31U
#define NIL UINT_MAX

#define BASE_OF(x) ((x) / ELEMENT_CARDINALITY)
#define UNIT_OF(x) (div32((x) % ELEMENT_CARDINALITY))
#define BIT_OF(x) (mod32(x))

#define cudaSafeCall(err) { \
if (err != cudaSuccess) { \
printf("Runtime API error: %s.\n", cudaGetErrorString(err)); \
exit(-1); \
} \
}

__device__ __host__ inline uint mul32(uint num) {
	return num << 5;
}

__device__ __host__ inline uint div32(uint num) {
	return num >> 5;
}

__device__ __host__ inline uint mod32(uint num) {
	return num & 31;
}

__device__ __host__ inline uint mul960(uint num) {
	// 960 = 1024 - 64
	return (num << 10) - (num << 6);
}

// e.g. for powerOfTwo = 32: 4 => 32, 32 => 32, 33 => 64
// second parameter has to be a power of two
__device__ __host__ inline uint roundToNextMultipleOf(uint num, uint powerOfTwo = 32) {
	if ((num & (powerOfTwo - 1)) == 0) {
		return num;
	}
	return (num / powerOfTwo + 1) * powerOfTwo;
}

// e.g. for powerOfTwo = 32: 0 => 0, 4 => 0, 32 => 32, 33 => 32
// second parameter has to be a power of two
__device__ __host__ inline uint roundToPrevMultipleOf(uint num, uint powerOfTwo = 32) {
	if ((num & (powerOfTwo - 1)) == 0) {
		return num;
	}
	return ((num / powerOfTwo + 1) * powerOfTwo) - 32;
}

__device__ __host__ inline uint getElapsedTime(const clock_t startTime) {
	return (clock() - startTime) / (CLOCKS_PER_SEC * 60);
}

struct Partition {
	// interval
	uint firstVar, lastVar;
	// num of edges/size of graph per partition
	uint oldSize, deltaSize, tmpSize;
	// edge array or sparse bit vector
	bool flag;
	Partition(uint a, uint b, uint c, uint d, uint e, bool f) : firstVar(a), lastVar(b), oldSize(c), deltaSize(d), tmpSize(e), flag(f) {}
};

struct Degree {
	uint numEdges, numElements;
	Degree(uint a, uint b) : numEdges(a), numElements(b) {}
};

struct ComputeRegion {
	uint firstVar;
	uint lastVar;
	short start;
	uint offset;
	bool flag;
	ComputeRegion(uint a, uint b, short s, uint o, bool f) : firstVar(a), lastVar(b), start(s), offset(o), flag(f) {}
};

__host__ inline int getNumRegions(Partition p) {
	int n = 0;
	if (p.oldSize != 0) {
		n++;
	}
	if (p.deltaSize != 0) {
		n++;
	}
	if (p.tmpSize != 0) {
		n++;
	}
	return n;
}

void transferRules(uint numRules, uint numLabels, uint* rules);

void transferElements(Partition p, uint* elements, short start, uint* elementPool, uint heapSize);

uint* allocateElementPool(size_t totalDeviceMemoryInBytes, uint &heapSize);

void spagpu_s(Partition &p, bool &r, short start, uint numLabels, uint heapSize);

void spagpu_b1(Partition &p1, Partition p2, bool &r1, uint numLabels, uint heapSize);

void spagpu_b2(Partition p1, Partition &p2, bool &r2, uint numLabels, uint heapSize);

void getDegree(Partition p, short start, std::vector<Degree> &degree);

void mergeAndDiff(Partition &p, uint* elementPool, uint numLabels, uint heapSize, short start1, short start2);
