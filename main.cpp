//
// main.cpp
// spagpu_v2
//
// Created by LuS on 2018/4/17.
//

#include <fstream>
#include <sstream>
#include <map>
#include <set>

#include "kernel.h"

using namespace std;

void checkGPUConfiguration() {
	int deviceCount;
	cudaGetDeviceCount(&deviceCount);
	// cout << "NUM COMPUTE-CAPABLE DEVICES: " << deviceCount << endl;
	if (deviceCount == 0) {
		cerr << "There is no device supporting CUDA." << endl;
		exit(-1);
	}
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);
	if (deviceProp.warpSize != WARP_SIZE) {
		cerr << "Warp size must be 32." << endl;
		exit(-1);
	}
	if (deviceProp.kernelExecTimeoutEnabled) {
		cerr << "There is a run time limit on kernels." << endl;
		exit(-1);
	}
}

size_t getDeviceMemory() {
	size_t curAvailDeviceMemoryInBytes, totalDeviceMemoryInBytes;
	cudaMemGetInfo(&curAvailDeviceMemoryInBytes, &totalDeviceMemoryInBytes);
	cout << "GPU's total memory: " << B2MB(totalDeviceMemoryInBytes) << " MB, free Memory: " << B2MB(curAvailDeviceMemoryInBytes) << " MB." << endl;
	return curAvailDeviceMemoryInBytes;
}

string skipBlanksAndComments(ifstream &fin) {
	string line;
	for (;;) {
		getline(fin, line);
		if (!line.empty() && line[0] != '#') {
			return line;
		}
	}
}

string nextItem(istringstream &lineStream) {
	string item;
	getline(lineStream, item, '\t');
	return item;
}

void readAndTransferRules(char *filePath, map<string, uint> &labelMap) {
	ifstream fin;
	fin.open(filePath);
	string line = skipBlanksAndComments(fin);
	uint numRules = atoi(line.c_str());
	cout << "NUM RULES: " << numRules << endl;
	uint* rules = new uint[numRules * 3];
	for (int i = 0; i < numRules * 3; i++) {
		rules[i] = 0;
	}
	int k = 1;
	for (int i = 0; i < numRules; i++) {
		string line = skipBlanksAndComments(fin);
		istringstream lineStream(line);
		int j = 0;
		string item;
		while (getline(lineStream, item, '\t')) {
			if (labelMap.count(item) == 0) {
				labelMap.insert(pair<string, uint>(item, k));
				k++;
			}
			rules[i * 3 + j] = labelMap[item];
			j++;
		}
	}
	uint numLabels = labelMap.size();
	cout << "NUM LABELS: " << numLabels << endl;
	fin.close();
	transferRules(numRules, numLabels, rules);
	delete[] rules;
}

void getGraphInfo(char *filePath, vector<Degree> &degree, map<string, uint> labelMap) {
	ifstream fin;
	fin.open(filePath);
	string line = skipBlanksAndComments(fin);
	istringstream lineStream(line);
	uint numEdges = atoi(nextItem(lineStream).c_str());
	uint numVars = atoi(nextItem(lineStream).c_str());
	cout << "NUM VERTICES: " << numVars << endl;
	cout << "INITIAL NUM EDGES: " << numEdges << endl;
	for (int i = 0; i < numVars; i++) {
		degree.push_back(Degree(0, 0));
	}
	set<pair<uint, uint>> s;
	uint tmp = 0;
	for (int i = 0; i < numEdges; i++) {
		string line = skipBlanksAndComments(fin);
		istringstream lineStream(line);
		uint src = atoi(nextItem(lineStream).c_str());
		degree[src].numEdges++;
		if (src != tmp) {
			degree[tmp].numElements = s.size();
			s.clear();
			tmp = src;
		}
		uint dst = atoi(nextItem(lineStream).c_str());
		uint base = BASE_OF(dst);
		uint rel = labelMap[nextItem(lineStream)];
		s.insert(pair<uint, uint>(base, rel));
	}
	degree[tmp].numElements = s.size();
	s.clear();
	fin.close();
}

void initializePartitions(uint heapSize, vector<Degree> degree, vector<Partition> &partitions, uint numLabels) {
	uint partitionSize = heapSize / 8 * 3;
	uint numVars = degree.size();
	uint tmp1 = 0;
	uint tmp2 = 0;
	uint tmp3 = 0;
	for (int i = 0; i < numVars; i++) {
		tmp1 += degree[i].numElements;
		tmp2 += degree[i].numEdges;
		if (tmp1 * ELEMENT_WIDTH + roundToNextMultipleOf(i - tmp3 + 1) * numLabels > partitionSize) {
			partitions.push_back(Partition(tmp3, i - 1, 0, tmp2 - degree[i].numEdges, 0, false));
			tmp1 = degree[i].numElements;
			tmp2 = degree[i].numEdges;
			tmp3 = i;
		}
	}
	partitions.push_back(Partition(tmp3, numVars - 1, 0, tmp2, 0, false));
	// for small graphs
	if (partitions.size() == 1) {
		uint tmp4 = 0;
		uint tmp5 = 0;
		for (int i = 0; i < numVars; i++) {
			tmp4 += degree[i].numElements;
			tmp5 += degree[i].numEdges;
			if (tmp4 > tmp1 / 2) {
				partitions[0].lastVar = i - 1;
				partitions[0].deltaSize = tmp5 - degree[i].numEdges;
				tmp3 = i;
				break;
			}
		}
		partitions.push_back(Partition(tmp3, numVars - 1, 0, tmp2 - partitions[0].deltaSize, 0, false));
	}
	cout << "INITIAL NUM PARTITIONS: " << partitions.size() << endl;
}

uint* readEdges(char *filePath, map<string, uint> labelMap, Partition p, uint* &valIndex, uint &numKeys) {
	cout << "Reading Edges..." << flush;
	ifstream fin;
	fin.open(filePath);
	string line = skipBlanksAndComments(fin);
	istringstream lineStream(line);
	uint numEdges = atoi(nextItem(lineStream).c_str());
	nextItem(lineStream);
	uint* edges = new uint[p.deltaSize * 3];
	uint k = 0;
	for (int i = 0; i < numEdges; i++) {
		string line = skipBlanksAndComments(fin);
		istringstream lineStream(line);
		uint src = atoi(nextItem(lineStream).c_str());
		if (src < p.firstVar) {
			nextItem(lineStream);
			nextItem(lineStream);
		}
		else if (src >= p.firstVar && src <= p.lastVar) {
			edges[k] = src;
			uint dst = atoi(nextItem(lineStream).c_str());
			edges[k + p.deltaSize] = dst;
			uint rel = labelMap[nextItem(lineStream)];
			edges[k + p.deltaSize * 2] = rel;
			k++;
		}
		else {
			break;
		}
	}
	fin.close();
	valIndex = new uint[p.deltaSize + 1];
	valIndex[0] = 0;
	k = 1;
	for (int i = 1; i < p.deltaSize; i++) {
		if (edges[i] != edges[i - 1]) {
			valIndex[k] = i;
			edges[k] = edges[i];
			k++;
		}
	}
	numKeys = k;
	valIndex[numKeys] = p.deltaSize;
	cout << "OK." << endl;
	return edges;
}

uint getIndex(uint headIndex, uint* elements, uint &k) {
	uint index = elements[headIndex];
	if (index == NIL) {
		uint newIndex = k;
		k += ELEMENT_WIDTH;
		elements[headIndex] = newIndex;
		elements[newIndex + BASE] = NIL;
		return newIndex;
	}
	return index;
}

void insert(uint index, const uint var, uint* elements, uint &k) {
	uint base = BASE_OF(var);
	uint unit = UNIT_OF(var);
	uint bit = BIT_OF(var);
	uint myBits = 1 << bit;
	for (;;) {
		uint toBase = elements[index + BASE];
		if (toBase == NIL) {
			elements[index + BASE] = base;
			elements[index + unit] = myBits;
			elements[index + NEXT] = NIL;
			return;
		}
		if (toBase == base) {
			elements[index + unit] |= myBits;
			return;
		}
		if (toBase < base) {
			uint toNext = elements[index + NEXT];
			if (toNext == NIL) {
				uint newIndex = k;
				k += ELEMENT_WIDTH;
				elements[index + NEXT] = newIndex;
				elements[newIndex + BASE] = base;
				elements[newIndex + unit] = myBits;
				elements[newIndex + NEXT] = NIL;
				return;
			}
			index = toNext;
		}
		else {
			uint newIndex = k;
			k += ELEMENT_WIDTH;
			for (int i = 0; i < ELEMENT_WIDTH; i++) {
				elements[newIndex + i] = elements[index + i];
				elements[index + i] = 0;
			}
			elements[index + BASE] = base;
			elements[index + unit] = myBits;
			elements[index + NEXT] = newIndex;
			return;
		}
	}
}

uint* createGraph(Partition &p, uint numLabels, vector<Degree> degree, uint* edges, uint* valIndex, uint numKeys) {
	cout << "Creating Graph..." << flush;
	uint numElements = 0;
	for (int i = p.firstVar; i <= p.lastVar; i++) {
		numElements += degree[i].numElements;
	}
	uint virtualNumVars = roundToNextMultipleOf(p.lastVar - p.firstVar + 1);
	uint headSize = virtualNumVars * numLabels;
	uint* elements = new uint[headSize + numElements * ELEMENT_WIDTH]();
	uint k = 0;
	for (int i = 0; i < headSize; i++) {
		elements[i] = NIL;
	}
	k += headSize;
	for (int i = 0; i < numKeys; i++) {
		uint src = edges[i];
		uint begin = valIndex[i];
		uint end = valIndex[i + 1];
		for (int j = begin; j < end; j++) {
			uint dst = edges[p.deltaSize + j];
			uint rel = edges[p.deltaSize * 2 + j];
			uint headIndex = virtualNumVars * (rel - 1) + src - p.firstVar;
			uint index = getIndex(headIndex, elements, k);
			insert(index, dst, elements, k);
		}
	}
	p.deltaSize = k;
	p.flag = true;
	delete[] edges;
	delete[] valIndex;
	cout << "OK." << endl;
	return elements;
}

uint* readElements(Partition p, int k) {
	string str = "Partition" + to_string(k);
	FILE *fp = fopen(str.c_str(), "rb");
	uint poolSize = p.oldSize + p.deltaSize + p.tmpSize;
	uint* elements = new uint[poolSize];
	fread(elements, uintSize, poolSize, fp);
	fclose(fp);
	return elements;
}

void readAndTransferElements(Partition p, int k, short start, uint heapSize, uint* elementPool) {
	cout << "Reading Elements..." << flush;
	uint* elements = readElements(p, k);
	cout << "OK." << endl;
	cout << "Transferring Elements..." << flush;
	transferElements(p, elements, start, elementPool, heapSize);
	cout << "OK." << endl;
}

uint* transferBackElements(Partition p, short start, uint* elementPool, uint heapSize) {
	uint poolSize = p.oldSize + p.deltaSize + p.tmpSize;
	uint* elements = new uint[poolSize];
	size_t size = poolSize * uintSize;
	cudaSafeCall(cudaMemcpy(elements, elementPool + (heapSize / 2) * start, size, D2H));
	return elements;
}

void storeElements(Partition p, int k, uint* elements) {
	string str = "Partition" + to_string(k);
	FILE *fp = fopen(str.c_str(), "wb+");
	uint poolSize = p.oldSize + p.deltaSize + p.tmpSize;
	fwrite(elements, uintSize, poolSize, fp);
	fclose(fp);
	delete[] elements;
}

void transferBackAndStoreElements(Partition p, int k, short start, uint* elementPool, uint heapSize) {
	cout << "Transferring Back Elements..." << flush;
	uint* elements = transferBackElements(p, start, elementPool, heapSize);
	cout << "OK." << endl;
	cout << "Storing Elements..." << flush;
	storeElements(p, k, elements);
	cout << "OK." << endl;
}

void repartRegion(uint* fromElements, uint fromOffset, uint* toElements1, uint* toElements2, uint &k1, uint &k2, uint numVars1, uint numVars2, uint numLabels) {
	uint virtualNumVars1 = roundToNextMultipleOf(numVars1);
	uint virtualNumVars2 = roundToNextMultipleOf(numVars2);
	uint virtualNumVars = roundToNextMultipleOf(numVars1 + numVars2);
	uint toOffset1 = k1;
	uint toOffset2 = k2;
	for (int i = 0; i < numLabels; i++) {
		memcpy(toElements1 + k1, fromElements + fromOffset + virtualNumVars * i, size_t(numVars1) * uintSize);
		for (int j = numVars1; j < virtualNumVars1; j++) {
			toElements1[k1 + j] = NIL;
		}
		k1 += virtualNumVars1;
		memcpy(toElements2 + k2, fromElements + fromOffset + virtualNumVars * i + numVars1, size_t(numVars2) * uintSize);
		for (int j = numVars2; j < virtualNumVars2; j++) {
			toElements2[k2 + j] = NIL;
		}
		k2 += virtualNumVars2;
	}
	for (int i = 0; i < numLabels; i++) {
		for (int j = 0; j < numVars1; j++) {
			uint fromHeadIndex = fromOffset + virtualNumVars * i + j;
			uint fromIndex = fromElements[fromHeadIndex];
			if (fromIndex != NIL) {
				uint toHeadIndex1 = toOffset1 + virtualNumVars1 * i + j;
				toElements1[toHeadIndex1] = k1;
				uint toIndex1 = k1;
				k1 += ELEMENT_WIDTH;
				for (;;) {
					memcpy(toElements1 + toIndex1, fromElements + fromIndex, ELEMENT_WIDTH * uintSize);
					fromIndex = fromElements[fromIndex + NEXT];
					if (fromIndex == NIL) {
						break;
					}
					toElements1[toIndex1 + NEXT] = k1;
					toIndex1 = k1;
					k1 += ELEMENT_WIDTH;
				}
			}
		}
		for (int j = 0; j < numVars2; j++) {
			uint fromHeadIndex = fromOffset + virtualNumVars * i + numVars1 + j;
			uint fromIndex = fromElements[fromHeadIndex];
			if (fromIndex != NIL) {
				uint toHeadIndex2 = toOffset2 + virtualNumVars2 * i + j;
				toElements2[toHeadIndex2] = k2;
				uint toIndex2 = k2;
				k2 += ELEMENT_WIDTH;
				for (;;) {
					memcpy(toElements2 + toIndex2, fromElements + fromIndex, ELEMENT_WIDTH * uintSize);
					fromIndex = fromElements[fromIndex + NEXT];
					if (fromIndex == NIL) {
						break;
					}
					toElements2[toIndex2 + NEXT] = k2;
					toIndex2 = k2;
					k2 += ELEMENT_WIDTH;
				}
			}
		}
	}
}

void repartElements(vector<Partition> &partitions, int k, uint* elements, uint* &elements1, uint* &elements2, vector<Degree> degree, uint numLabels) {
	cout << "Repartitioning..." << flush;
	int numRegions = getNumRegions(partitions[k]);
	uint headSize = roundToNextMultipleOf(partitions[k].lastVar - partitions[k].firstVar + 1) * numLabels;
	uint numElements = (partitions[k].oldSize + partitions[k].deltaSize + partitions[k].tmpSize - headSize * numRegions) / ELEMENT_WIDTH;
	uint tmp1 = 0;
	uint tmp2 = 0;
	uint tmp3 = partitions[k].lastVar;
	uint totalDegree1, totalDegree2;
	for (int i = partitions[k].firstVar; i <= partitions[k].lastVar; i++) {
		tmp1 += degree[i].numElements;
		if (tmp1 > numElements / 2) {
			partitions[k].lastVar = i - 1;
			totalDegree1 = tmp1 - (degree[i].numElements);
			totalDegree2 = numElements - totalDegree1;
			tmp2 = i;
			break;
		}
	}
	partitions.insert(partitions.begin() + k + 1, Partition(tmp2, tmp3, 0, 0, 0, true));
	uint deltaOffset = partitions[k].oldSize;
	uint tmpOffset = partitions[k].oldSize + partitions[k].deltaSize;
	uint numVars1 = partitions[k].lastVar - partitions[k].firstVar + 1;
	uint numVars2 = partitions[k + 1].lastVar - partitions[k + 1].firstVar + 1;
	elements1 = new uint[totalDegree1 * ELEMENT_WIDTH + roundToNextMultipleOf(numVars1) * numLabels * numRegions];
	elements2 = new uint[totalDegree2 * ELEMENT_WIDTH + roundToNextMultipleOf(numVars2) * numLabels * numRegions];
	uint k1 = 0;
	uint k2 = 0;
	if (partitions[k].oldSize != 0) {
		repartRegion(elements, 0, elements1, elements2, k1, k2, numVars1, numVars2, numLabels);
		partitions[k].oldSize = k1;
		partitions[k + 1].oldSize = k2;
	}
	if (partitions[k].deltaSize != 0) {
		repartRegion(elements, deltaOffset, elements1, elements2, k1, k2, numVars1, numVars2, numLabels);
		partitions[k].deltaSize = k1 - partitions[k].oldSize;
		partitions[k + 1].deltaSize = k2 - partitions[k + 1].oldSize;
	}
	if (partitions[k].tmpSize != 0) {
		repartRegion(elements, tmpOffset, elements1, elements2, k1, k2, numVars1, numVars2, numLabels);
		partitions[k].tmpSize = k1 - partitions[k].oldSize - partitions[k].deltaSize;
		partitions[k + 1].tmpSize = k2 - partitions[k + 1].oldSize - partitions[k + 1].deltaSize;
	}
	delete[] elements;
	cout << "OK." << endl;
}

void renamePartitions(uint numPartitions, int k) {
	for (int i = numPartitions - 1; i > k; i--) {
		std::string oldName, newName;
		oldName = "Partition" + std::to_string(i);
		newName = "Partition" + std::to_string(i + 1);
		rename(oldName.c_str(), newName.c_str());
	}
}

void repartition(vector<Partition> &partitions, int k, vector<Degree> &degree, uint* elementPool, uint numLabels, uint heapSize, short start) {
	getDegree(partitions[k], start, degree);
	uint* elements = transferBackElements(partitions[k], start, elementPool, heapSize);
	uint *elements1, *elements2;
	repartElements(partitions, k, elements, elements1, elements2, degree, numLabels);
	transferElements(partitions[k], elements1, start, elementPool, heapSize);
	renamePartitions(partitions.size(), k);
	storeElements(partitions[k + 1], k + 1, elements2);
}

void run_computation(char *filePath, map<string, uint> labelMap, vector<Partition> &partitions, vector<Degree> &degree, uint heapSize, uint* elementPool) {
	for (int i = 0; i < partitions.size(); i++) {
		bool r1 = false;
		// load Partition p1
		if (partitions[i].flag == false) { // edge array
			uint *valIndex, numKeys;
			uint* edges = readEdges(filePath, labelMap, partitions[i], valIndex, numKeys);
			uint* elements = createGraph(partitions[i], labelMap.size(), degree, edges, valIndex, numKeys);
			transferElements(partitions[i], elements, 0, elementPool, heapSize);
		}
		else {
			readAndTransferElements(partitions[i], i, 0, heapSize, elementPool);
		}

		// self-matching
		do {
			if (r1) {
				repartition(partitions, i, degree, elementPool, labelMap.size(), heapSize, 0);
				r1 = false;
			}
			spagpu_s(partitions[i], r1, 0, labelMap.size(), heapSize);
		} while (r1);

		// avoid duplicates
		int c1 = 1;
		for (;;) {
			for (int j = i + 1; j < partitions.size(); j++) {
				bool r2 = false;
				// load Partition p2
				if (partitions[j].flag == false) {
					uint *valIndex, numKeys;
					uint* edges = readEdges(filePath, labelMap, partitions[j], valIndex, numKeys);
					uint* elements = createGraph(partitions[j], labelMap.size(), degree, edges, valIndex, numKeys);
					transferElements(partitions[j], elements, 1, elementPool, heapSize);
				}
				else {
					readAndTransferElements(partitions[j], j, 1, heapSize, elementPool);
				}

				// apply rules from p1 to p2
				do {
					if (r1) {
						repartition(partitions, i, degree, elementPool, labelMap.size(), heapSize, 0);
						r1 = false;
						j++;
						c1++;
					}
					spagpu_b1(partitions[i], partitions[j], r1, labelMap.size(), heapSize);
				} while (r1);

				// avoid duplicates
				int c2 = 1;
				for (;;) {
					// apply rules from p2 to p1
					do {
						if (r2) {
							repartition(partitions, j, degree, elementPool, labelMap.size(), heapSize, 1);
							r2 = false;
							c2++;
						}
						spagpu_b2(partitions[i], partitions[j], r2, labelMap.size(), heapSize);
					} while (r2);

					transferBackAndStoreElements(partitions[j], j, 1, elementPool, heapSize);

					c2--;
					if (c2 == 0) {
						break;
					}

					j++;
					readAndTransferElements(partitions[j], j, 1, heapSize, elementPool);
				}
			}
			// update
			mergeAndDiff(partitions[i], elementPool, labelMap.size(), heapSize, 0, 1);

			transferBackAndStoreElements(partitions[i], i, 0, elementPool, heapSize);

			c1--;
			if (c1 == 0) {
				break;
			}

			i++;
			readAndTransferElements(partitions[i], i, 0, heapSize, elementPool);
		}
	}
}

int popcnt(uint num)
{
	int r = 0;
	while (num)
	{
		num &= num - 1;
		r++;
	}
	return r;
}

void getNumEdges(Partition p, uint* elements, uint &numEdges, uint numLabels) {
	uint numVars = p.lastVar - p.firstVar + 1;
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	for (int i = 0; i < numVars; i++) {
		for (int j = 0; j < numLabels; j++) {
			uint headIndex = virtualNumVars * j + i;
			uint index = elements[headIndex];
			if (index != NIL) {
				do {
					for (int i = 0; i < 30; i++) {
						uint myBits = elements[index + i];
						if (myBits) {
							numEdges += popcnt(myBits);
						}
					}
					index = elements[index + NEXT];
				} while (index != NIL);
			}
		}
	}
	delete[] elements;
}

int main(int argc, char** argv) {

	clock_t startTime = clock();

	// PREPROCESSING
	cout << "===== PREPROCESSING INFO =====" << endl;
	checkGPUConfiguration();

	map<string, uint> labelMap;
	readAndTransferRules(argv[1], labelMap);

	vector<Degree> degree; // num of edges/elements per vertex
	getGraphInfo(argv[2], degree, labelMap);

	size_t totalDeviceMemoryInBytes = getDeviceMemory();
	// Amount of memory reserved for the graph
	// It has to be slightly smaller than the total amount of available device memory.
	// in units of uintSize
	uint heapSize;
	uint* elementPool = allocateElementPool(totalDeviceMemoryInBytes, heapSize);
	// divide vertices into logical intervals
	vector<Partition> partitions;
	initializePartitions(heapSize, degree, partitions, labelMap.size());
	
	// COMPUTATION
	int roundNo = 0;
	for (;;) {
		cout << "##### ROUND " << ++roundNo << " #####" << endl;
		run_computation(argv[2], labelMap, partitions, degree, heapSize, elementPool);

		// The computation is finished when no new edges can be added.
		bool done = true;
		for (int i = 0; i < partitions.size(); i++) {
			if (partitions[i].deltaSize != 0) {
				done = false;
			}
		}
		if (done) {
			cudaFree(elementPool);
			break;
		}
	}

	// POSTPROCESSING
	uint numEdges = 0;
	for (int i = 0; i < partitions.size(); i++) {
		uint* elements = readElements(partitions[i], i);
		getNumEdges(partitions[i], elements, numEdges, labelMap.size());
	}

	cout << "===== SPAGPU FINISHED =====" << endl;
	cout << "NUM ROUNDS: " << roundNo << endl;
	cout << "FINAL NUM PARTITIONS: " << partitions.size() << endl;
	cout << "FINAL NUM EDGES: " << numEdges << endl;
	cout << "TOTAL SPAGPU TIME: " << getElapsedTime(startTime) << "min." << endl;

	return 0;

}
