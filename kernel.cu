#include "kernel.h"

const uint N = 20;

__device__ __constant__ uint dev_numLabels;
__device__ __constant__ uint dev_numRules;
__device__ __constant__ uint dev_rules[N * 3];
__device__ __constant__ uint dev_heapSize;
__device__ __constant__ uint* dev_elementPool;
// index of the next free element in the corresponding free list
// The index is given in units of uintSize.
__device__ uint freeList1 = 0;
__device__ uint freeList2 = 0;
__device__ uint counter = 0;
__device__ uint worklistIndex = 0;
__device__ bool dev_r = false;

uint* allocateElementPool(size_t totalDeviceMemoryInBytes, uint &heapSize) {
	uint* elementPool;
	heapSize = B2MB(totalDeviceMemoryInBytes / 8 * 7) * 1024 * 256;
	std::cout << "HEAP SIZE: " << heapSize << " (in units of uintSize)" << std::endl;
	cudaSafeCall(cudaMalloc((void **)&elementPool, size_t(heapSize) * uintSize));
	cudaSafeCall(cudaMemcpyToSymbol(dev_heapSize, &heapSize, uintSize));
	cudaSafeCall(cudaMemcpyToSymbol(dev_elementPool, &elementPool, sizeof(uint*)));
	return elementPool;
}

void transferRules(uint numRules, uint numLabels, uint* rules) {
	cudaSafeCall(cudaMemcpyToSymbol(dev_numLabels, &numLabels, uintSize));
	cudaSafeCall(cudaMemcpyToSymbol(dev_numRules, &numRules, uintSize));
	if (numRules > N) {
		std::cerr << "N needs to be reset." << std::endl;
		exit(-1);
	}
	cudaSafeCall(cudaMemcpyToSymbol(dev_rules, rules, numRules * uintSize * 3));
}

void transferElements(Partition p, uint* elements, short start, uint* elementPool, uint heapSize) {
	uint poolSize = p.oldSize + p.deltaSize + p.tmpSize;
	if (start) {
		cudaSafeCall(cudaMemcpyToSymbol(freeList2, &poolSize, uintSize));
	}
	else {
		cudaSafeCall(cudaMemcpyToSymbol(freeList1, &poolSize, uintSize));
	}
	size_t size = poolSize * uintSize;
	cudaSafeCall(cudaMemcpy(elementPool + (heapSize / 2) * start, elements, size, H2D));
	delete[] elements;
}

__host__ inline uint getBlocks() {
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);
	return deviceProp.multiProcessorCount;
}

__device__ inline uint getThreadIdInBlock() {
	return threadIdx.x + threadIdx.y * blockDim.x;
}

__device__ inline uint isFirstThreadOfBlock() {
	return !getThreadIdInBlock();
}

__device__ inline uint isFirstThreadOfWarp() {
	return !threadIdx.x;
}

__device__ inline uint getWarpsPerGrid() {
	return blockDim.y * gridDim.x;
}

__device__ inline uint getWarpIdInGrid() {
	return (blockIdx.x * (blockDim.x * blockDim.y / WARP_SIZE) + threadIdx.y);
}

__device__ inline void graphSet(const uint pos, const uint val) {
	dev_elementPool[pos] = val;
}

__device__ inline uint graphGet(const uint pos) {
	return dev_elementPool[pos];
}

__device__ uint getValAtThread(const uint myVal, const uint i) {
	__shared__ volatile uint temp[THREADS_PER_BLOCK / WARP_SIZE];
	if (threadIdx.x == i) {
		temp[threadIdx.y] = myVal;
	}
	return temp[threadIdx.y];
}

__device__ inline uint getAndIncrement(const uint delta) {
	__shared__ volatile uint temp[THREADS_PER_BLOCK / WARP_SIZE];
	if (isFirstThreadOfWarp()) {
		temp[threadIdx.y] = atomicAdd(&worklistIndex, delta);
	}
	return temp[threadIdx.y];
}

__device__ inline void resetWorklistIndex() {
	__syncthreads();
	if (isFirstThreadOfBlock() && atomicInc(&counter, gridDim.x - 1) == (gridDim.x - 1)) {
		worklistIndex = 0;
	}
}

__device__ void syncAllThreads() {
	__syncthreads();
	if (isFirstThreadOfBlock()) {
		volatile uint* p = &counter;
		if (atomicInc(&counter, gridDim.x - 1) < gridDim.x - 1) {
			while (*p); // spinning...
		}
	}
	__syncthreads();
}

__device__ inline uint mallocIn(short start, uint size = ELEMENT_WIDTH) {
	__shared__ volatile uint temp[THREADS_PER_BLOCK / WARP_SIZE];
	if (isFirstThreadOfWarp()) {
		if (start) {
			temp[threadIdx.y] = atomicAdd(&freeList2, size);
		}
		else {
			temp[threadIdx.y] = atomicAdd(&freeList1, size);
		}
	}
	if (temp[threadIdx.y] + size > dev_heapSize / 2) {
		dev_r = true;
		return -1;
	}
	else {
		return temp[threadIdx.y];
	}
}

__global__ void initialize(uint virtualNumVars, short start, uint offset) {
	uint headSize = virtualNumVars * dev_numLabels;
	if (isFirstThreadOfBlock()) {
		if (start) {
			freeList2 = offset + headSize;
		}
		else {
			freeList1 = offset + headSize;
		}
	}
	__syncthreads();
	uint inc = getWarpsPerGrid();
	uint init = getWarpIdInGrid();
	uint startIndex = (dev_heapSize / 2) * start;
	for (int i = init; i < headSize / WARP_SIZE; i += inc) {
		uint headIndex = startIndex + offset + mul32(i);
		graphSet(headIndex + threadIdx.x, NIL);
	}
	syncAllThreads();
}

__device__ inline uint getIndex(uint headIndex, short start) {
	uint index = graphGet(headIndex);
	if (index == NIL) {
		uint newIndex = mallocIn(start);
		if (newIndex != -1) {
			graphSet(headIndex, newIndex);
			graphSet((dev_heapSize / 2) * start + newIndex + threadIdx.x, NIL);
		}
		return newIndex;
	}
	return index;
}

__device__ uint addElement(uint index, const uint fromBase, const uint fromBits, short start) {
	uint startIndex = (dev_heapSize / 2) * start;
	for (;;) {
		uint toBits = graphGet(index + threadIdx.x);
		uint toBase = graphGet(index + BASE);
		if (toBase == NIL) {
			// can only happen if the list is empty
			graphSet(index + threadIdx.x, fromBits);
			return index;
		}
		if (toBase == fromBase) {
			uint orBits = toBits | fromBits;
			if (orBits != toBits && threadIdx.x < NEXT) {
				graphSet(index + threadIdx.x, orBits);
			}
			return index;
		}
		if (toBase < fromBase) {
			uint toNext = getValAtThread(toBits, NEXT);
			if (toNext == NIL) {
				// appending
				uint newIndex = mallocIn(start);
				if (newIndex == -1) {
					return -1;
				}
				graphSet(newIndex + startIndex + threadIdx.x, fromBits);
				graphSet(index + NEXT, newIndex);
				return newIndex + startIndex;
			}
			index = toNext + startIndex;
		}
		else {
			uint newIndex = mallocIn(start);
			if (newIndex == -1) {
				return -1;
			}
			graphSet(newIndex + startIndex + threadIdx.x, toBits);
			uint val = threadIdx.x == NEXT ? newIndex : fromBits;
			graphSet(index + threadIdx.x, val);
			return index;
		}
	}
}

__device__ uint insert(uint index, const uint var, short start) {
	uint base = BASE_OF(var);
	uint unit = UNIT_OF(var);
	uint bit = BIT_OF(var);
	uint myBits = 0;
	if (threadIdx.x == unit) {
		myBits = 1 << bit;
	}
	else if (threadIdx.x == BASE) {
		myBits = base;
	}
	else if (threadIdx.x == NEXT) {
		myBits = NIL;
	}
	return addElement(index, base, myBits, start);
}

/*
__global__ void addEdges(uint* keys, uint* valIndex, const uint numKeys, uint* val1, uint* val2, uint firstVar, uint lastVar, short start, uint offset) {
	__shared__ uint temp[THREADS_PER_BLOCK / WARP_SIZE * 64];
	uint* p = &temp[threadIdx.y * 64];
	uint startIndex = (dev_heapSize / 2) * start;
	uint virtualNumVars = roundToNextMultipleOf(lastVar - firstVar + 1);
	uint i = getAndIncrement(1);
	while (i < numKeys) {
		uint src = keys[i];
		uint begin = valIndex[i];
		uint end = valIndex[i + 1];
		uint virtualBegin = roundToPrevMultipleOf(begin); // to ensure alignment
		for (int j = virtualBegin; j < end; j += WARP_SIZE) {
			uint myIndex = j + threadIdx.x;
			p[threadIdx.x] = myIndex < end ? val1[myIndex] : NIL;
			p[threadIdx.x + 32] = myIndex < end ? val2[myIndex] : NIL;
			uint beginK = max((int)begin - j, 0);
			uint endK = min(end - j, WARP_SIZE);
			for (int k = beginK; k < endK; k++) {
				uint dst = p[k];
				uint rel = p[k + 32];
				uint headIndex = startIndex + offset + virtualNumVars * (rel - 1) + src - firstVar;
				uint index = getIndex(headIndex, start);
				if (index == -1) {
					break;
				}
				uint s = insert(index + startIndex, dst, start);
				if (s == -1) {
					break;
				}
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}
*/

__device__ uint clone(uint toIndex, short toStart, uint toStartIndex, uint fromBits, uint fromNext, uint fromStartIndex) {
	for (;;) {
		uint newIndex;
		if (fromNext == NIL) {
			newIndex = NIL;
		}
		else {
			newIndex = mallocIn(toStart);
			if (newIndex == -1) {
				uint val1 = threadIdx.x == NEXT ? NIL : fromBits;
				graphSet(toIndex + threadIdx.x, val1);
				return -1;
			}
		}
		uint val2 = threadIdx.x == NEXT ? newIndex : fromBits;
		graphSet(toIndex + threadIdx.x, val2);
		if (fromNext == NIL) {
			break;
		}
		toIndex = newIndex + toStartIndex;
		fromBits = graphGet(fromNext + fromStartIndex + threadIdx.x);
		fromNext = graphGet(fromNext + fromStartIndex + NEXT);
	}
	return 0;
}

__device__ uint union2(const uint to, const uint toRel, ComputeRegion tmp, const uint fromIndex, const short fromStart) {
	uint fromStartIndex = (dev_heapSize / 2) * fromStart;
	uint toStartIndex = (dev_heapSize / 2) * tmp.start;
	uint fromBits = graphGet(fromIndex + threadIdx.x);
	uint fromBase = graphGet(fromIndex + BASE);
	uint fromNext = graphGet(fromIndex + NEXT);
	uint headIndex = toStartIndex + tmp.offset + roundToNextMultipleOf(tmp.lastVar - tmp.firstVar + 1) * (toRel - 1) + to;
	uint toIndex = getIndex(headIndex, tmp.start);
	if (toIndex == -1) {
		return -1;
	}
	toIndex += toStartIndex;
	uint toBits = graphGet(toIndex + threadIdx.x);
	uint toBase = graphGet(toIndex + BASE);
	if (toBase == NIL) {
		uint s = clone(toIndex, tmp.start, toStartIndex, fromBits, fromNext, fromStartIndex);
		if (s == -1) {
			return -1;
		}
		return 0;
	}
	uint toNext = graphGet(toIndex + NEXT);
	for (;;) {
		if (toBase > fromBase) {
			uint newIndex = mallocIn(tmp.start);
			if (newIndex == -1) {
				return -1;
			}
			graphSet(newIndex + toStartIndex + threadIdx.x, toBits);
			uint val = threadIdx.x == NEXT ? newIndex : fromBits;
			graphSet(toIndex + threadIdx.x, val);
			if (fromNext == NIL) {
				return 0;
			}
			toIndex = newIndex + toStartIndex;
			fromBits = graphGet(fromNext + fromStartIndex + threadIdx.x);
			fromBase = graphGet(fromNext + fromStartIndex + BASE);
			fromNext = graphGet(fromNext + fromStartIndex + NEXT);
		}
		else if (toBase == fromBase) {
			uint newToNext;
			if (toNext == NIL && fromNext != NIL) {
				newToNext = mallocIn(tmp.start);
				if (newToNext == -1) {
					uint orBits = fromBits | toBits;
					uint newBits = threadIdx.x == NEXT ? toNext : orBits;
					if (newBits != toBits) {
						graphSet(toIndex + threadIdx.x, newBits);
					}
					return -1;
				}
			}
			else {
				newToNext = toNext;
			}
			uint orBits = fromBits | toBits;
			uint newBits = threadIdx.x == NEXT ? newToNext : orBits;
			if (newBits != toBits) {
				graphSet(toIndex + threadIdx.x, newBits);
			}
			if (fromNext == NIL) {
				return 0;
			}
			fromBits = graphGet(fromNext + fromStartIndex + threadIdx.x);
			fromBase = graphGet(fromNext + fromStartIndex + BASE);
			fromNext = graphGet(fromNext + fromStartIndex + NEXT);
			if (toNext == NIL) {
				uint s = clone(newToNext + toStartIndex, tmp.start, toStartIndex, fromBits, fromNext, fromStartIndex);
				if (s == -1) {
					return -1;
				}
				return 0;
			}
			toIndex = toNext + toStartIndex;
			toBits = graphGet(toIndex + threadIdx.x);
			toBase = graphGet(toIndex + BASE);
			toNext = graphGet(toIndex + NEXT);
		}
		else {
			if (toNext == NIL) {
				toNext = mallocIn(tmp.start);
				if (toNext == -1) {
					return -1;
				}
				graphSet(toIndex + NEXT, toNext);
				uint s = clone(toNext + toStartIndex, tmp.start, toStartIndex, fromBits, fromNext, fromStartIndex);
				if (s == -1) {
					return -1;
				}
				return 0;
			}
			toIndex = toNext + toStartIndex;
			toBits = graphGet(toIndex + threadIdx.x);
			toBase = graphGet(toIndex + BASE);
			toNext = graphGet(toIndex + NEXT);
		}
	}
}

__device__ uint unionAll(uint toRel, uint fromRel, const uint to, uint* p, uint numFrom, ComputeRegion dst1, ComputeRegion dst2, ComputeRegion tmp) {
	uint startIndex_dst1 = (dev_heapSize / 2) * dst1.start;
	uint virtualNumVars_dst1 = roundToNextMultipleOf(dst1.lastVar - dst1.firstVar + 1);
	uint startIndex_dst2 = (dev_heapSize / 2) * dst2.start;
	uint virtualNumVars_dst2 = roundToNextMultipleOf(dst2.lastVar - dst2.firstVar + 1);
	for (int i = 0; i < numFrom; i++) {
		if (p[i] >= dst1.firstVar && p[i] <= dst1.lastVar) {
			uint headIndex = startIndex_dst1 + dst1.offset + virtualNumVars_dst1 * (fromRel - 1) + p[i] - dst1.firstVar;
			uint fromIndex = graphGet(headIndex);
			if (fromIndex != NIL) {
				uint s = union2(to, toRel, tmp, fromIndex + startIndex_dst1, dst1.start);
				if (s == -1) {
					return -1;
				}
			}
		}
		if (dst2.flag) {
			if (p[i] >= dst2.firstVar && p[i] <= dst2.lastVar) {
				uint headIndex = startIndex_dst2 + dst2.offset + virtualNumVars_dst2 * (fromRel - 1) + p[i] - dst2.firstVar;
				uint fromIndex = graphGet(headIndex);
				if (fromIndex != NIL) {
					uint s = union2(to, toRel, tmp, fromIndex + startIndex_dst2, dst2.start);
					if (s == -1) {
						return -1;
					}
				}
			}
		}
	}
	return 0;
}

__device__  uint decode(uint toRel, uint fromRel, const uint i, const uint base, const uint myBits, uint* p, ComputeRegion dst1, ComputeRegion dst2, ComputeRegion tmp) {
	uint nonEmpty = __ballot(myBits) & ((1 << 30) - 1);
	const uint threadMask = 1 << threadIdx.x;
	const uint myMask = threadMask - 1;
	const uint base960 = mul960(base);
	while (nonEmpty) {
		uint pos = __ffs(nonEmpty) - 1;
		nonEmpty &= (nonEmpty - 1);
		uint bits = getValAtThread(myBits, pos);
		uint var = base960 + mul32(pos) + threadIdx.x;
		uint bitActive = bits & threadMask;
		uint numOnes = __popc(bits);
		pos = __popc(bits & myMask);
		if (bitActive) {
			p[pos] = var;
		}
		uint s = unionAll(toRel, fromRel, i, p, numOnes, dst1, dst2, tmp);
		if (s == -1) {
			return -1;
		}
	}
	return 0;
}

__device__ uint apply(const uint firstRel, const uint secondRel, const uint thirdRel, uint i, uint* p, ComputeRegion src, ComputeRegion dst1, ComputeRegion dst2, ComputeRegion tmp) {
	uint startIndex = (dev_heapSize / 2) * src.start;
	uint headIndex = startIndex + src.offset + roundToNextMultipleOf(src.lastVar - src.firstVar + 1) * (firstRel - 1) + i;
	uint index = graphGet(headIndex);
	if (index == NIL) {
		return 0;
	}
	do {
		index += startIndex;
		uint myBits = graphGet(index + threadIdx.x);
		uint base = graphGet(index + BASE);
		uint s = decode(thirdRel, secondRel, i, base, myBits, p, dst1, dst2, tmp);
		if (s == -1) {
			return -1;
		}
		index = graphGet(index + NEXT);
	} while (index != NIL);
	return 0;
}

// for complete rules
__global__ void compute11(ComputeRegion src, ComputeRegion dst1, ComputeRegion dst2, ComputeRegion tmp) {
	__shared__ uint temp[THREADS_PER_BLOCK / WARP_SIZE * 32];
	uint* p = &temp[threadIdx.y * 32];
	uint numVars = src.lastVar - src.firstVar + 1;
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numRules; j++) {
			if (dev_rules[j * 3 + 1] != 0 && dev_rules[j * 3 + 2] != 0) {
				uint s = apply(dev_rules[j * 3 + 1], dev_rules[j * 3 + 2], dev_rules[j * 3], i, p, src, dst1, dst2, tmp);
				if (s == -1) {
					break;
				}
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

// for rules which have two labels
__global__ void compute10(uint firstVar, uint lastVar, uint newOffset, ComputeRegion tmp, short start) {
	uint startIndex = (dev_heapSize / 2) * start;
	uint numVars = lastVar - firstVar + 1;
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numRules; j++) {
			if (dev_rules[j * 3 + 1] != 0 && dev_rules[j * 3 + 2] == 0) {
				uint headIndex = startIndex + newOffset + virtualNumVars * (dev_rules[j * 3 + 1] - 1) + i;
				uint fromIndex = graphGet(headIndex);
				if (fromIndex != NIL) {
					uint s = union2(i, dev_rules[j * 3], tmp, fromIndex + startIndex, start);
					if (s == -1) {
						break;
					}
				}
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

// for rules which have only one label
__global__ void compute00(uint firstVar, uint lastVar, uint tmpOffset, short start) {
	uint startIndex = (dev_heapSize / 2) * start;
	uint numVars = lastVar - firstVar + 1;
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numRules; j++) {
			if (dev_rules[j * 3 + 1] == 0) {
				uint headIndex = startIndex + tmpOffset + virtualNumVars * (dev_rules[j * 3] - 1) + i;
				uint toIndex = getIndex(headIndex, start);
				if (toIndex == -1) {
					break;
				}
				uint s = insert(toIndex + startIndex, firstVar + i, start);
				if (s == -1) {
					break;
				}
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

void spagpu_s(Partition &p, bool &r, short start, uint numLabels, uint heapSize) {
	if (p.deltaSize != 0) {
		std::cout << "Self-matching..." << std::flush;
		uint blocks = getBlocks();
		dim3 threads(WARP_SIZE, THREADS_PER_BLOCK / WARP_SIZE);
		if (p.tmpSize == 0) {
			uint numVars = p.lastVar - p.firstVar + 1;
			uint virtualNumVars = roundToNextMultipleOf(numVars);
			uint headSize = virtualNumVars * numLabels;
			uint offset = p.oldSize + p.deltaSize;
			if (offset + headSize > heapSize / 2) { // if the size of the partition exceeds the limit, return and repart
				r = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
			initialize<<<blocks, threads>>>(virtualNumVars, start, offset);
		}
		if (p.oldSize == 0) {
			compute00<<<blocks, threads>>>(p.firstVar, p.lastVar, p.deltaSize, start);
			bool s = false;
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p.tmpSize = heapSize / 2 - p.deltaSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
		}
		ComputeRegion tmp_s(p.firstVar, p.lastVar, start, p.oldSize + p.deltaSize, true);
		compute10<<<blocks, threads>>>(p.firstVar, p.lastVar, p.oldSize, tmp_s, start);
		bool s = false;
		cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
		if (s) {
			p.tmpSize = heapSize / 2 - p.deltaSize - p.oldSize;
			s = false;
			cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
			r = true;
			std::cout << "Need Repartition." << std::endl;
			return;
		}
		ComputeRegion new_s(p.firstVar, p.lastVar, start, p.oldSize, true);
		ComputeRegion empty(0, 0, 0, 0, false);
		if (p.oldSize != 0) {
			ComputeRegion old_s(p.firstVar, p.lastVar, start, 0, true);
			compute11<<<blocks, threads>>>(old_s, new_s, empty, tmp_s);
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p.tmpSize = heapSize / 2 - p.deltaSize - p.oldSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
			compute11<<<blocks, threads>>>(new_s, old_s, new_s, tmp_s);
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p.tmpSize = heapSize / 2 - p.deltaSize - p.oldSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
		}
		else {
			compute11<<<blocks, threads>>>(new_s, new_s, empty, tmp_s);
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p.tmpSize = heapSize / 2 - p.deltaSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
		}
		uint poolSize;
		if (start) {
			cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList2, uintSize));
		}
		else {
			cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList1, uintSize));
		}
		p.tmpSize = poolSize - p.deltaSize - p.oldSize;
		std::cout << "OK." << std::endl;
	}
}

void spagpu_b1(Partition &p1, Partition p2, bool &r1, uint numLabels, uint heapSize) {
	if (p1.deltaSize != 0 || p2.deltaSize != 0) {
		std::cout << "Solving (from p1 to p2)..." << std::flush;
		uint blocks = getBlocks();
		dim3 threads(WARP_SIZE, THREADS_PER_BLOCK / WARP_SIZE);
		if (p1.tmpSize == 0) {
			uint numVars = p1.lastVar - p1.firstVar + 1;
			uint virtualNumVars = roundToNextMultipleOf(numVars);
			uint headSize = virtualNumVars * numLabels;
			uint offset = p1.oldSize + p1.deltaSize;
			if (offset + headSize > heapSize / 2) {
				r1 = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
			initialize<<<blocks, threads>>>(virtualNumVars, 0, offset);
		}
		ComputeRegion empty(0, 0, 0, 0, false);
		ComputeRegion tmp1(p1.firstVar, p1.lastVar, 0, p1.oldSize + p1.deltaSize, true);
		bool s = false;
		if (p1.oldSize != 0 && p2.deltaSize != 0) {
			ComputeRegion old1(p1.firstVar, p1.lastVar, 0, 0, true);
			ComputeRegion new2(p2.firstVar, p2.lastVar, 1, p2.oldSize, true);
			compute11<<<blocks, threads>>>(old1, new2, empty, tmp1);
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p1.tmpSize = heapSize / 2 - p1.deltaSize - p1.oldSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r1 = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
		}
		if (p1.deltaSize != 0) {
			ComputeRegion new1(p1.firstVar, p1.lastVar, 0, p1.oldSize, true);
			if (p2.oldSize != 0 && p2.deltaSize != 0) {
				ComputeRegion old2(p2.firstVar, p2.lastVar, 1, 0, true);
				ComputeRegion new2(p2.firstVar, p2.lastVar, 1, p2.oldSize, true);
				compute11<<<blocks, threads>>>(new1, old2, new2, tmp1);
				cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
				if (s) {
					p1.tmpSize = heapSize / 2 - p1.deltaSize - p1.oldSize;
					s = false;
					cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
					r1 = true;
					std::cout << "Need Repartition." << std::endl;
					return;
				}
			}
			else {
				if (p2.oldSize != 0) {
					ComputeRegion old2(p2.firstVar, p2.lastVar, 1, 0, true);
					compute11<<<blocks, threads>>>(new1, old2, empty, tmp1);
					cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
					if (s) {
						p1.tmpSize = heapSize / 2 - p1.deltaSize - p1.oldSize;
						s = false;
						cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
						r1 = true;
						std::cout << "Need Repartition." << std::endl;
						return;
					}
				}
				if (p2.deltaSize != 0) {
					ComputeRegion new2(p2.firstVar, p2.lastVar, 1, p2.oldSize, true);
					compute11<<<blocks, threads>>>(new1, new2, empty, tmp1);
					cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
					if (s) {
						p1.tmpSize = heapSize / 2 - p1.deltaSize - p1.oldSize;
						s = false;
						cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
						r1 = true;
						std::cout << "Need Repartition." << std::endl;
						return;
					}
				}
			}
		}
		uint poolSize;
		cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList1, uintSize));
		p1.tmpSize = poolSize - p1.deltaSize - p1.oldSize;
		std::cout << "OK." << std::endl;
	}
}

void spagpu_b2(Partition p1, Partition &p2, bool &r2, uint numLabels, uint heapSize) {
	if (p1.deltaSize != 0 || p2.deltaSize != 0) {
		std::cout << "Solving (from p2 to p1)..." << std::flush;
		uint blocks = getBlocks();
		dim3 threads(WARP_SIZE, THREADS_PER_BLOCK / WARP_SIZE);
		if (p2.tmpSize == 0) {
			uint numVars = p2.lastVar - p2.firstVar + 1;
			uint virtualNumVars = roundToNextMultipleOf(numVars);
			uint headSize = virtualNumVars * numLabels;
			uint offset = p2.oldSize + p2.deltaSize;
			if (offset + headSize > heapSize / 2) {
				r2 = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
			initialize<<<blocks, threads>>>(virtualNumVars, 1, offset);
		}
		ComputeRegion empty(0, 0, 0, 0, false);
		ComputeRegion tmp2(p2.firstVar, p2.lastVar, 1, p2.oldSize + p2.deltaSize, true);
		bool s = false;
		if (p2.oldSize != 0 && p1.deltaSize != 0) {
			ComputeRegion old2(p2.firstVar, p2.lastVar, 1, 0, true);
			ComputeRegion new1(p1.firstVar, p1.lastVar, 0, p1.oldSize, true);
			compute11<<<blocks, threads>>>(old2, new1, empty, tmp2);
			cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
			if (s) {
				p2.tmpSize = heapSize / 2 - p2.deltaSize - p2.oldSize;
				s = false;
				cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
				r2 = true;
				std::cout << "Need Repartition." << std::endl;
				return;
			}
		}
		if (p2.deltaSize != 0) {
			ComputeRegion new2(p2.firstVar, p2.lastVar, 1, p2.oldSize, true);
			if (p1.oldSize != 0 && p1.deltaSize != 0) {
				ComputeRegion old1(p1.firstVar, p1.lastVar, 0, 0, true);
				ComputeRegion new1(p1.firstVar, p1.lastVar, 0, p1.oldSize, true);
				compute11<<<blocks, threads>>>(new2, old1, new1, tmp2);
				cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
				if (s) {
					p2.tmpSize = heapSize / 2 - p2.deltaSize - p2.oldSize;
					s = false;
					cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
					r2 = true;
					std::cout << "Need Repartition." << std::endl;
					return;
				}
			}
			else {
				if (p1.oldSize != 0) {
					ComputeRegion old1(p1.firstVar, p1.lastVar, 0, 0, true);
					compute11<<<blocks, threads>>>(new2, old1, empty, tmp2);
					cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
					if (s) {
						p2.tmpSize = heapSize / 2 - p2.deltaSize - p2.oldSize;
						s = false;
						cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
						r2 = true;
						std::cout << "Need Repartition." << std::endl;
						return;
					}
				}
				if (p1.deltaSize != 0) {
					ComputeRegion new1(p1.firstVar, p1.lastVar, 0, p1.oldSize, true);
					compute11<<<blocks, threads>>>(new2, new1, empty, tmp2);
					cudaSafeCall(cudaMemcpyFromSymbol(&s, dev_r, sizeof(bool)));
					if (s) {
						p2.tmpSize = heapSize / 2 - p2.deltaSize - p2.oldSize;
						s = false;
						cudaSafeCall(cudaMemcpyToSymbol(dev_r, &s, sizeof(bool)));
						r2 = true;
						std::cout << "Need Repartition." << std::endl;
						return;
					}
				}
			}
		}
		uint poolSize;
		cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList2, uintSize));
		p2.tmpSize = poolSize - p2.deltaSize - p2.oldSize;
		std::cout << "OK." << std::endl;
	}
}

__device__ void computeDegreePerLabel(uint* degree, const uint i, uint index, const uint startIndex) {
	do {
		if (isFirstThreadOfWarp()) {
			degree[i]++;
		}
		index = graphGet(index + startIndex + NEXT);
	} while (index != NIL);
}

__global__ void computeDegree(uint* degree, uint numVars, short start, uint offset) {
	uint startIndex = (dev_heapSize / 2) * start;
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numLabels; j++) {
			uint headIndex = startIndex + offset + virtualNumVars * j + i;
			uint index = graphGet(headIndex);
			if (index != NIL) {
				computeDegreePerLabel(degree, i, index, startIndex);
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

void getDegree(Partition p, short start, std::vector<Degree> &degree) {
	uint numVars = p.lastVar - p.firstVar + 1;
	uint* host_degree = new uint[numVars];
	for (int i = 0; i < numVars; i++) {
		host_degree[i] = 0;
	}
	uint* dev_degree;
	size_t size = numVars * uintSize;
	cudaSafeCall(cudaMalloc((void **)&dev_degree, size));
	cudaSafeCall(cudaMemcpy(dev_degree, host_degree, size, H2D));
	uint blocks = getBlocks();
	dim3 threads(WARP_SIZE, THREADS_PER_BLOCK / WARP_SIZE);
	if (p.oldSize != 0) {
		computeDegree<<<blocks, threads>>>(dev_degree, numVars, start, 0);
	}
	if (p.deltaSize != 0) {
		computeDegree<<<blocks, threads>>>(dev_degree, numVars, start, p.oldSize);
	}
	if (p.tmpSize != 0) {
		computeDegree<<<blocks, threads>>>(dev_degree, numVars, start, p.oldSize + p.deltaSize);
	}
	cudaSafeCall(cudaMemcpy(host_degree, dev_degree, size, D2H));
	cudaFree(dev_degree);
	for (int i = 0; i < numVars; i++) {
		degree[p.firstVar + i].numElements = host_degree[i];
	}
	delete[] host_degree;
}

__global__ void merge(ComputeRegion old_s, uint fromOffset, short fromStart) {
	uint numVars = old_s.lastVar - old_s.firstVar + 1;
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	uint fromStartIndex = (dev_heapSize / 2) * fromStart;
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numLabels; j++) {
			uint headIndex = fromStartIndex + fromOffset + virtualNumVars * j + i;
			uint fromIndex = graphGet(headIndex);
			if (fromIndex != NIL) {
				union2(i, j + 1, old_s, fromIndex + fromStartIndex, fromStart);
			}
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

__device__ void removeDuplicates(uint subHeadIndex, uint toHeadIndex, uint myBase, uint myBits, short toStart) {
	uint toStartIndex = (dev_heapSize / 2) * toStart;
	uint subIndex = graphGet(subHeadIndex);
	if (subIndex == NIL) {
		uint toIndex = getIndex(toHeadIndex, toStart);
		addElement(toIndex + toStartIndex, myBase, myBits, toStart);
		return;
	}
	subIndex += toStartIndex;
	uint subBits = graphGet(subIndex + threadIdx.x);
	uint subBase = graphGet(subIndex + BASE);
	uint subNext = graphGet(subIndex + NEXT);
	for (;;) {
		if (subBase > myBase) {
			uint toIndex = getIndex(toHeadIndex, toStart);
			addElement(toIndex + toStartIndex, myBase, myBits, toStart);
			return;
		}
		else if (subBase == myBase) {
			if (threadIdx.x < BASE) {
				myBits = ~subBits & myBits;
			}
			uint nonEmpty = __ballot(myBits) & ((1 << 30) - 1);
			if (nonEmpty) {
				uint toIndex = getIndex(toHeadIndex, toStart);
				addElement(toIndex + toStartIndex, myBase, myBits, toStart);
			}
			return;
		}
		else {
			if (subNext == NIL) {
				uint toIndex = getIndex(toHeadIndex, toStart);
				addElement(toIndex + toStartIndex, myBase, myBits, toStart);
				return;
			}
			subIndex = subNext + toStartIndex;
			subBits = graphGet(subIndex + threadIdx.x);
			subBase = graphGet(subIndex + BASE);
			subNext = graphGet(subIndex + NEXT);
		}
	}
}

__device__ void computeDiff(uint subHeadIndex, uint fromIndex, uint toHeadIndex, short toStart, short fromStart) {
	uint fromStartIndex = (dev_heapSize / 2) * fromStart;
	do {
		fromIndex += fromStartIndex;
		uint myBits = graphGet(fromIndex + threadIdx.x);
		uint myBase = graphGet(fromIndex + BASE);
		if (threadIdx.x == NEXT) {
			myBits = NIL;
		}
		removeDuplicates(subHeadIndex, toHeadIndex, myBase, myBits, toStart);
		fromIndex = graphGet(fromIndex + NEXT);
	} while (fromIndex != NIL);
}

__global__ void diff(uint numVars, uint toOffset, short toStart, uint fromOffset, short fromStart) {
	uint virtualNumVars = roundToNextMultipleOf(numVars);
	uint fromStartIndex = (dev_heapSize / 2) * fromStart;
	uint toStartIndex = (dev_heapSize / 2) * toStart;
	uint i = getAndIncrement(1);
	while (i < numVars) {
		for (int j = 0; j < dev_numLabels; j++) {
			uint fromHeadIndex = fromStartIndex + fromOffset + virtualNumVars * j + i;
			uint fromIndex = graphGet(fromHeadIndex);
			if (fromIndex == NIL) {
				continue;
			}
			uint subHeadIndex = toStartIndex + virtualNumVars * j + i;
			uint toHeadIndex = toStartIndex + toOffset + virtualNumVars * j + i;
			computeDiff(subHeadIndex, fromIndex, toHeadIndex, toStart, fromStart);
		}
		i = getAndIncrement(1);
	}
	resetWorklistIndex();
}

void mergeAndDiff(Partition &p, uint* elementPool, uint numLabels, uint heapSize, short start1, short start2) {
	std::cout << "Updating..." << std::flush;
	uint blocks = getBlocks();
	dim3 threads(WARP_SIZE, THREADS_PER_BLOCK / WARP_SIZE);
	uint oldSize = p.oldSize;
	uint newSize = p.deltaSize;
	uint tmpSize = p.tmpSize;
	if (newSize != 0) {
		if (oldSize == 0) {
			p.oldSize = newSize;
			p.deltaSize = 0;
		}
		else {
			size_t size = newSize * uintSize;
			cudaSafeCall(cudaMemcpy(elementPool + (heapSize / 2) * start2 + oldSize, elementPool + (heapSize / 2) * start1 + oldSize, size, D2D));
			uint poolSize = oldSize;
			if (start1) {
				cudaSafeCall(cudaMemcpyToSymbol(freeList2, &poolSize, uintSize));
			}
			else {
				cudaSafeCall(cudaMemcpyToSymbol(freeList1, &poolSize, uintSize));
			}
			ComputeRegion old_s(p.firstVar, p.lastVar, start1, 0, true);
			merge<<<blocks, threads>>>(old_s, oldSize, start2);
			if (start1) {
				cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList2, uintSize));
			}
			else {
				cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList1, uintSize));
			}
			p.oldSize = poolSize;
			p.deltaSize = 0;
		}
	}
	if (tmpSize != 0) {
		size_t size = tmpSize * uintSize;
		uint fromOffset = oldSize + newSize;
		cudaSafeCall(cudaMemcpy(elementPool + (heapSize / 2) * start2 + fromOffset, elementPool + (heapSize / 2) * start1 + fromOffset, size, D2D));
		uint numVars = p.lastVar - p.firstVar + 1;
		uint virtualNumVars = roundToNextMultipleOf(numVars);
		initialize<<<blocks, threads>>>(virtualNumVars, start1, p.oldSize);
		diff<<<blocks, threads>>>(numVars, p.oldSize, start1, fromOffset, start2);
		uint poolSize;
		if (start1) {
			cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList2, uintSize));
		}
		else {
			cudaSafeCall(cudaMemcpyFromSymbol(&poolSize, freeList1, uintSize));
		}
		if (poolSize - p.oldSize == virtualNumVars * numLabels) {
			poolSize = p.oldSize;
			if (start1) {
				cudaSafeCall(cudaMemcpyToSymbol(freeList2, &poolSize, uintSize));
			}
			else {
				cudaSafeCall(cudaMemcpyToSymbol(freeList1, &poolSize, uintSize));
			}
		}
		else {
			p.deltaSize = poolSize - p.oldSize;
		}
		p.tmpSize = 0;
	}
	std::cout << "OK." << std::endl;
}
